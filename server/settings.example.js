const curr_url = process.env.ROOT_URL;
if (typeof curr_url === "undefined") {
  process.env.ROOT_URL = "http://localhost:3000";
}

const sender_email = process.env.SENDER_EMAIL;
if (typeof sender_email === "undefined") {
  process.env.SENDER_EMAIL = "";
}

const admin_email = process.env.ADMIN_EMAIL;
if (typeof admin_email === "undefined") {
  process.env.ADMIN_EMAIL = "";
}

const mail_url = process.env.MAIL_URL;
if (typeof mail_url === "undefined") {
  process.env.MAIL_URL = "";
}

const fb_id = process.env.FB_ID;
if (typeof fb_id === "undefined") {
  process.env.FB_ID = "";
}

const fb_secret = process.env.FB_SECRET;
if (typeof fb_secret === "undefined") {
  process.env.FB_SECRET = "";
}

const paypal_env = process.env.PAYPAL_ENV;
if (typeof paypal_sandbox === "undefined") {
  process.env.PAYPAL_ENV = "sandbox";
}
